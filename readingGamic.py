import pyart
import numpy as np
import matplotlib.pyplot as plt
import glob
import matplotlib.colors as colors

filenames = glob.glob('./*.nc')
filenames=['STI-400--2018-08-28--11-16-27.mvol']
filenames.sort()

indexVar =  0
indexElev = 0

for filename in filenames:
    radar = pyart.aux_io.gamic_hdf5.read_gamic(filename)
    radarOptsToPlot = {'vars': [key for key in radar.fields.keys()],
                       'elev': radar.sweep_number['data']}
    display = pyart.graph.RadarMapDisplay(radar)
    map_panel_axes = [0.12, 0.18, 0.7, 0.7]
    fig = plt.figure(figsize=[8, 8])
    ax = fig.add_axes(map_panel_axes)
    display.plot_ppi_map(radarOptsToPlot['vars'][indexVar], sweep=radarOptsToPlot['elev'][indexElev], 
                         vmin=10, vmax=65,
                         min_lon=radar.longitude['data'][0]-1, max_lon=radar.longitude['data'][0]+1, 
                         min_lat=radar.latitude['data'][0]-1,  max_lat=radar.latitude['data'][0]+1,
                         projection='lcc', lon_lines=np.arange(-62., -46., .5),
                         lat_lines=np.arange(-30., -20., .5), lat_0=radar.latitude['data'][0],
                         lon_0=radar.longitude['data'][0], cmap='pyart_NWSRef',mask_outside=True,
                         shapefile='MUNICIPIOS_polígonos')

    plt.show()